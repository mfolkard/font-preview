package com.fontpreview.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class OSCommand {
    /**
     * The String representation of the path to the command that is to be run, e.g. /usr/bin/gradle
     */
    private String command;

    /**
     * The file that the command is to be run on
     */
    private String subject;

    /**
     * The arguments to the command
     */
    private List<String> arguments;

    private OSCommand(Builder builder){
        this.command = builder.command;
        this.subject = builder.subject;
        this.setArguments(builder.arguments);
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    private List<String> getArguments() {
        return arguments;
    }

    private void setArguments(List<String> arguments) {
        if(arguments != null) {
            this.arguments = arguments;
            this.arguments.add(0, this.command);
            this.arguments.add(this.arguments.size(), this.subject);
        }
    }

    /**
     * Runs the command and returns the result as a string
     *
     * @return The output of the command as a String
     */
    public String run() {
        try {
            System.out.println("Running:" + getArguments().toString());
            ProcessBuilder pb = new ProcessBuilder(getArguments());
            pb.redirectErrorStream(true);
			Process proc = pb.start();
            InputStream is = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            
            StringBuilder output_buffer = new StringBuilder();

            while ((line = br.readLine()) != null) {
                output_buffer.append(line);
            }
            //System.out.println("Finished:" + arguments.toString());
            return output_buffer.toString();
        } catch (Exception e) {
            System.out.println("Error running command:" + e.getMessage());
        }
        return "";
    }

    public static class Builder {
        private String command;
        private String subject;
        private List<String> arguments;

        public Builder(){
            arguments = new ArrayList<>();
        }

        public Builder withCommand(String command){
            this.command = command;
            return this;
        }

        public Builder withSubject(String subject){
            this.subject = subject;
            return this;
        }

        public Builder withArgument(String argument){
            this.arguments.add(argument);
            return this;
        }

        public Builder withArguments(List<String> arguments){
            this.arguments= arguments;
            return this;
        }

        public OSCommand build(){
            return new OSCommand(this);
        }

    }
}
