package com.fontpreview.service;

import com.fontpreview.dto.FontDto;
import com.fontpreview.util.OSCommand;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static org.jooq.generated.tables.Font.FONT;

@RestController
@EnableConfigurationProperties
@RequestMapping(value={"/${app.version}/font"})
class FontService  {

    @Autowired
	private DSLContext dsl;

    @Value("${app.upload-directory}")
	private String uploadDirectory;

    @Value("${app.command.exiftool}")
    private String exifTool;

    /**
     * Upload a font to the system.
     * @param font The font file
     * @param request The HTTP request
     * @return The record for the font just uploaded
     * @throws Exception
     */
	@PostMapping("/")
	public Object uploadFont(@RequestParam("file") MultipartFile font,
            HttpServletRequest request) throws Exception {
        createUploadDirectory();
        File uploadedFile = getUploadedFileName(font);
        writeUpload(font, uploadedFile);

        return insertFontRecord(font.getOriginalFilename(), uploadedFile.getAbsolutePath());
	}

    /**
     * Returns the info for the given font id. This includes the EXIF information for the font.
     * @param id The id of the font that you want to get information for
     * @param extended This will return the extended/exiftool information with the font info
     * @return The font record
     * @throws Exception
     */
	@RequestMapping("/{id}")
    public FontDto getFont(
                              @PathVariable Integer id,
                              @RequestParam(value = "extended", required = false, defaultValue="true") boolean extended) throws Exception {
        List<FontDto> fonts = dsl
                .selectFrom(FONT)
                .where(FONT.ID.equal(id))
                .fetchInto(FontDto.class);
        if (fonts.isEmpty()){
            throw new Exception("No fonts found for that ID");
        }
        FontDto font = fonts.get(0);
        if(extended){
            OSCommand exif = new OSCommand.Builder()
                    .withCommand(this.exifTool)
                    .withSubject(font.getFilepath())
                    .withArgument("-a")
                    .withArgument("-j")
                    .build();
    
            font.setInfo(exif.run());
        }
        return font;
    }

    /**
     * Return the list of fonts in the database
     * @return The list of fonts
     * @throws Exception
     */
    @RequestMapping("/")
    public Object getFonts() throws Exception{
        List<FontDto> fonts = dsl
                .selectFrom(FONT)
                .fetchInto(FontDto.class);
        return fonts;
    }

    /**
     * Inserts a font into the database with the given filename and path
     * @param filename The filename of the font
     * @param filepath The path to the file
     * @return The database record that has just been inserted
     * @throws Exception If the font could not be inserted
     */
	private FontDto insertFontRecord(String filename, String filepath) throws Exception {
        Integer newFontId = dsl
                .insertInto(FONT)
                .set(FONT.NAME, filename)
                .set(FONT.FILEPATH, filepath)
                .returning(FONT.ID)
                .fetchOne()
                .getValue(FONT.ID);
        System.out.println("result is " + newFontId);

        List<FontDto> f = dsl
                .selectFrom(FONT)
                .where(FONT.ID.equal(newFontId))
                .fetchInto(FontDto.class);
        if(f.isEmpty()){
            throw new Exception("Could not insert Font record");
        }
        return f.get(0);
    }

    /**
     * Writes an uploaded file (from) to the given file location (to)
     * @param from The file that you wish to copy from
     * @param to The file that you wish to write to
     * @throws IOException If there was an issue writing to the file
     */
	private void writeUpload(MultipartFile from, File to) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(to)){
            fos.write(from.getBytes());
        }
    }

    /**
     * Returns the filepath for an uploaded file
     * @param font The font which was uploaded
     * @return The path to the uploaded file
     */
	private File getUploadedFileName(MultipartFile font){
        String filename = font.getOriginalFilename();
        System.out.println("font.getOriginalFilename() = " + filename);
        File uploadedFile = new File(uploadDirectory + "/" + filename);
        System.out.println("uploadedFile.getAbsolutePath() = " + uploadedFile.getAbsolutePath());
        return uploadedFile;
    }

    /**
     * Creates the upload directory if it doesn't exist
     * @throws Exception If the directory could not be created.
     */
	private void createUploadDirectory() throws Exception {
        File upload = new File(this.uploadDirectory);
        System.out.println("The upload folder is:" + upload.getAbsolutePath());
        if(!upload.exists()){
            if(!upload.mkdirs()){
                throw new Exception("Upload directory does not exist and cannot be created");
            }
        }

    }

}
