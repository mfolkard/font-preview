package com.fontpreview.service;

import com.fontpreview.dto.PreviewDto;
import com.fontpreview.dto.FontDto;
import com.fontpreview.util.OSCommand;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableConfigurationProperties
@RequestMapping(value={"/${app.version}/fontPreview"})
class FontPreviewService {

    @Autowired
    private DSLContext dsl;

    @Autowired
    private PreviewService previewService;

    @Autowired
    private FontService fontService;

    @Value("${app.command.convert}")
    private String imageMagickTool;


    @RequestMapping("/font/{fontId}/preview/{previewId}")
    public Object getFontPreviews(@PathVariable Integer fontId,
                                  @PathVariable Integer previewId) {
        return null;
    }

    @RequestMapping("/font/{fontId}/")
    public Object getFonts(@PathVariable Integer fontId) {
        return null;
    }

    @RequestMapping("/preview/{previewId}/")
    public Object getPreviews(@PathVariable Integer previewId) {
        return null;
    }

    @PostMapping("/font/{fontId}/preview/{previewId}")
    public Object createFontPreview(@PathVariable Integer fontId,
                                    @PathVariable Integer previewId) throws Exception{
        PreviewDto preview = this.previewService.getPreview(previewId);
        FontDto font = this.fontService.getFont(fontId, false);

        System.out.println("Font ID is " + font.getId());
        System.out.println("Preview ID is " + preview.getId());

        String outputFile = "/home/mark/tmp/fp-img.png";
        
		OSCommand fontPreview = new OSCommand.Builder()
            .withCommand(this.imageMagickTool)
            .withSubject(outputFile) /*convert -background '#272822' -fill '#f9f9f9' -font UbuntuMono-R.ttf -pointsize 10 -size 796x498 label:"$CODE" code.png*/
            .withArgument("-background")
            .withArgument("#" + preview.getBgcolour())
            .withArgument("-fill")
            .withArgument("#" + preview.getTextcolour())
            .withArgument("-font")
            .withArgument(font.getFilepath())
            .withArgument("-pointsize")
            .withArgument("26")
            .withArgument("-size")
            .withArgument(preview.getWidth() + "x" + preview.getHeight())
            .withArgument("label:\"" + preview.getText() + "\"")
            .build();

        String output = fontPreview.run();
        System.out.println("Command output was:\n" + output);
        return null;
    }

    @PostMapping("/font/{fontId}")
    public Object createFontPreview(@PathVariable Integer FontId,
                                    @RequestBody PreviewDto preview){
        return null;
    }


}
