package com.fontpreview.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;

import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.Result;
import org.jooq.impl.DSL;
import static org.jooq.generated.tables.Preview.PREVIEW;
import org.jooq.generated.tables.records.PreviewRecord;
import com.fontpreview.dto.PreviewDto;

@RestController
@EnableConfigurationProperties
@RequestMapping(value={"/${app.version}/preview"})
class PreviewService  {

    @Autowired
	private DSLContext dsl;

    @RequestMapping(value={"/"})
	public Object previews(
            HttpServletRequest request) {
        return dsl
            .selectFrom(PREVIEW)
            .fetchInto(PreviewDto.class);
	}

	@RequestMapping(value={"/{id}"})
	public PreviewDto getPreview(@PathVariable Integer id) {
        List<PreviewDto> previews = dsl
            .selectFrom(PREVIEW)
            .where(PREVIEW.ID.equal(id))
            .fetchInto(PreviewDto.class);
        if(!previews.isEmpty()){
            return previews.get(0);
        }
        return null;
	}

    @PostMapping("/")
    public Object createPreview(@RequestBody PreviewDto preview){
        PreviewRecord previewRecord = dsl.newRecord(PREVIEW, preview);
        previewRecord.store();
        return preview;
    }

}
