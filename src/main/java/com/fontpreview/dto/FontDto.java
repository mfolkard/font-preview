package com.fontpreview.dto;

public class FontDto {


    private Integer id; 
    
    private String name;

    private String filepath;

    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return this.id;
	}

    
	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}

    public void setFilepath(String filepath){
		this.filepath = filepath;
	}

	public String getFilepath(){
		return this.filepath;
	}


}
