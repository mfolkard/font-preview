package com.fontpreview.dto;

public class PreviewDto {


    private Integer id; 
    
    private String name; 
    
    private Integer width; 
    
    private Integer height; 
    
    private String bgcolour; 
    
    private String textcolour; 
    
    private String text; 
    
    private Integer overflow; 
    
    private Integer border; 
    
    private String bordercolour; 
    
    private Integer paddingleft; 
    
    private Integer paddingright; 
    
    private Integer paddingtop; 
    
    private Integer paddingbottom; 
    
    private String sizes; 
    
    private Integer imagetype; 

    
	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return this.id;
	}

    
	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}

    
	public void setWidth(Integer width){
		this.width = width;
	}

	public Integer getWidth(){
		return this.width;
	}

    
	public void setHeight(Integer height){
		this.height = height;
	}

	public Integer getHeight(){
		return this.height;
	}

    
	public void setBgcolour(String bgcolour){
		this.bgcolour = bgcolour;
	}

	public String getBgcolour(){
		return this.bgcolour;
	}

    
	public void setTextcolour(String textcolour){
		this.textcolour = textcolour;
	}

	public String getTextcolour(){
		return this.textcolour;
	}

    
	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return this.text;
	}

    
	public void setOverflow(Integer overflow){
		this.overflow = overflow;
	}

	public Integer getOverflow(){
		return this.overflow;
	}

    
	public void setBorder(Integer border){
		this.border = border;
	}

	public Integer getBorder(){
		return this.border;
	}

    
	public void setBordercolour(String bordercolour){
		this.bordercolour = bordercolour;
	}

	public String getBordercolour(){
		return this.bordercolour;
	}

    
	public void setPaddingleft(Integer paddingleft){
		this.paddingleft = paddingleft;
	}

	public Integer getPaddingleft(){
		return this.paddingleft;
	}

    
	public void setPaddingright(Integer paddingright){
		this.paddingright = paddingright;
	}

	public Integer getPaddingright(){
		return this.paddingright;
	}

    
	public void setPaddingtop(Integer paddingtop){
		this.paddingtop = paddingtop;
	}

	public Integer getPaddingtop(){
		return this.paddingtop;
	}

    
	public void setPaddingbottom(Integer paddingbottom){
		this.paddingbottom = paddingbottom;
	}

	public Integer getPaddingbottom(){
		return this.paddingbottom;
	}

    
	public void setSizes(String sizes){
		this.sizes = sizes;
	}

	public String getSizes(){
		return this.sizes;
	}

    
	public void setImagetype(Integer imagetype){
		this.imagetype = imagetype;
	}

	public Integer getImagetype(){
		return this.imagetype;
	}
}
