package com.fontpreview;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;


import javax.annotation.PostConstruct;
import org.hsqldb.util.DatabaseManagerSwing;


@ComponentScan({ "com.fontpreview" })
@Configuration
class FontPreviewConfig {
    
	@Autowired
    private
    DataSource dataSource;

	@Bean
	public JdbcTemplate getJdbcTemplate() {
		return new JdbcTemplate(dataSource);
    }

    /*
    @PostConstruct
	public void startDBManager() {
		DatabaseManagerSwing.main(new String[] { "--url", "jdbc:derby:~/pers/dev/font-preview/db/FontPreview;create=false", "--user", "sa", "--password", "sa" });
	}
    */
    
}
